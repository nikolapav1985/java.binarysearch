/**
*
* Class Bsearch
*
* used to perform binary search
*
* it takes log(2) n steps to find an item in a sorted array
*
* ----- references -----
*
* https://rosettacode.org/wiki/Binary_search
*
* ----- compile -----
*
* javac Bsearch.java
*
* ----- run example -----
*
* java Bsearch 5 15 18
*
* ----- example output -----
*
* 15 16 17 18 19 
* element found at position: 3
* 
* ----- compiler version -----
*
* javac version 11.0.5
*
**/
class Bsearch{
    public static void main(String[] args){
        int len=0;
        int min=0;
        int find=0;
        int foundindex=0;
        int i=0;
        int[] arr;
        int[] tmp;
        if(args.length != 3){
            System.exit(0);
        }
        len=Integer.parseInt(args[0]);
        min=Integer.parseInt(args[1]);
        find=Integer.parseInt(args[2]);
        arr=new int[len];
        tmp=new int[len];
        for(i=0;i<len;i++){
            arr[i]=min++;
        }
        for(i=0;i<len;i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println("");
        foundindex=bsearch(arr,find,0,len-1);
        System.out.format("element found at position: %d",foundindex);
        System.out.println("");
    }
    public static int bsearch(int[] arr,int find, int lo, int hi){ // lo - smallest array index
        // hi - highest array index
        // arr - array to be sorted
        // find - element to find
        int mid=0;
        for(;lo<=hi;){
            mid=(lo+hi)/2;
            if(arr[mid]==find){
                return mid;
            }
            if(find<arr[mid]){
                hi=mid-1;
            }
            if(find>arr[mid]){
                lo=mid+1;
            }
        }
        return -1;
    }
}

